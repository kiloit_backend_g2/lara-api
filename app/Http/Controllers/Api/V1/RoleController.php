<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(Request $request): \Illuminate\Database\Eloquent\Collection
    {
//        $roles = Role::orderBy('name', 'ASC');
//        if ($request->has('search') && !empty($request->search)) {
//            $roles = $roles->where('name', 'like', '%' . $request->search . '%');
//        }
//        $roles = $roles->paginate(10);
//        return Inertia::render('Role/Index', [
//            'roles' => $roles
//        ]);
        return Role::all();
    }

    public function create(Request $request): Response
    {
        return Inertia::render('Role/Create');
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $role = new Role();
        $role->fill($request->all())->save();

        return to_route('role.index');
    }

    public function edit($id): Response
    {
        return Inertia::render('Role/Edit', [
            'role' => Role::findById($id)
        ]);
    }

    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $role = Role::findOrFail($request->id);
        $role->fill($request->all())->save();

        return to_route('role.index');
    }

    public function destroy($id): RedirectResponse
    {
        Role::destroy($id);
        return to_route('role.index');
    }

    public function show($id)
    {
        return Role::findById($id);
    }
}
