<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $users = User::with('roles');
        if ($request->has('search') && !empty($request->search)) {
            $users = $users->where('name', 'like', '%' . $request->search . '%');
        }
        $users = $users->paginate(10);
        return response()->json([
            'users' => $users,

        ],200);
//        return User::all();
    }

    public function create(Request $request): Response
    {
        return Inertia::render('User/Create', [
            'roles' => Role::whereNotIn('name', ['Admin'])->pluck('name')
        ]);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'role' => 'required|string|max:255',
            'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = new User();
        $user->fill($request->all())->save();
        $user->assignRole($request->role);

//        return to_route('user.index');

        return response()->json([
            'user' => $user,

        ],200);
    }

    public function edit($id): Response
    {
        return Inertia::render('User/Edit', [
            'user' => User::with('roles')->find($id),
            'roles' => Role::whereNotIn('name', ['Admin'])->pluck('name')
        ]);
    }

//    public function edit($id): Response
//    {
//        return Inertia::render('User/Edit', [
//            'user' => User::with('roles')->find($id),
//            'roles' => Role::whereNotIn('name', ['Admin'])->pluck('name')
//        ]);
//    }

    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'role' => 'required|string|max:255',
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', Rule::unique(User::class)->ignore($request->id)],
        ]);

        $user = User::findOrFail($request->id);
        $user->fill($request->all())->save();

        if (!$user->hasRole($request->role))
            $user->syncRoles($request->role);

        return to_route('user.index');
    }

    public function destroy($id): RedirectResponse
    {
        User::destroy($id);
        return to_route('user.index');
    }
  public function show($id): \Illuminate\Http\JsonResponse
  {
//        return Inertia::render('User/Show', [
//            'user' => User::with('roles')->find($id)
//        ]);
      return response()->json([
            'user' => User::with('roles')->find($id),

        ],200);
    }
}
